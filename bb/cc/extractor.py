#!/usr/bin/env python
# encoding: utf-8
# vim: set syntax=python:

'''
This script extract the relevant information of statement of
BB's account
'''

import csv
from models import utils as U
from models import service as S
from models import transaction as T


def get_history_array(file_csv):
    '''
    Create a array from csv file
    '''

    array = []
    for row in file_csv:
        if not array or (not row and array[-1]):
            array.append(row)
        elif row != ['', '']:
            array[-1] = array[-1] + row

    header = array[0]
    tail = array[-1]

    array.remove(header)
    array.remove(tail)

    return array


def get_br_date(date):
    date = date.split('/')
    return date[1] + '/' + date[0] + '/' + date[2]


def get_name(name):
    name = U.strip_and_lower(name)

    if('saldo' in name) or ('s a l d o' in name):
        name = 'saldo'

    return name


def get_history_json(history_array):
    '''
    Create history JSON as history model
    '''

    children = []

    for history in history_array:
        description = 'transaction_cc'

        date = history[0]
        date = get_br_date(date) if date else None

        name = history[2]
        name = get_name(name) if name else None

        value = history[5]
        value = U.convert_to_float(value) if value else None

        ir = None
        iof = None
        quotes = None

        transaction = T.Transaction(
            [
                description,
                date,
                name,
                value,
                ir,
                iof,
                quotes
            ]
        )

        children.append(transaction.get_json())

    return children


def get_statement(document):
    name = 'cc'
    cnpj = None
    service_type = 'cc'

    file_csv = csv.reader(document)
    history_array = get_history_array(file_csv)
    history = get_history_json(history_array)

    service = S.Service(
        name,
        cnpj,
        service_type,
        history
    )

    return service.get_json()
