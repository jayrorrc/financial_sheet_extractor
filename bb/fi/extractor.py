#!/usr/bin/env python
# encoding: utf-8
# vim: set syntax=python:

'''
This script extract the relevant information of statement of
BB's investment funds
'''

import re
from bb.fi import history as H
from bb.fi import service as S

REGEX_NAME_FUND = r'=\s+BB\s.+'
REGEX_NAME_CNPJ = r'[0-9][0-9]\.[0-9][0-9][0-9]\.[0-9][0-9][0-9]\/[0-9][0-9][0-9][0-9]\-[0-9][0-9]'

REGEX_HISTORY = (
    r'(DATA.*VALOR COTA)'
    r'(((\s+\n+\s+)-+)\s+)'
    r'((Apl.+\s+|([0-3][0-9]\/[0-1][0-9]\/2[0-9]{3})'
    r'.*\s+)+)'
)


def get_histories_from_document(document):
    document_history = re.findall(REGEX_HISTORY, document)

    return map(H.get_history, document_history)


def get_service(fund_name, fund_cnpj):
    service = S.Service([fund_name, fund_cnpj])

    return service.get_json()


def get_services_from_document(document):
   document_services_names = re.findall(REGEX_NAME_FUND, document)
   document_services_cnpj = re.findall(REGEX_NAME_CNPJ, document)

    return map(get_service,document_services_names,document_services_cnpj)


def set_history_to_service(service, history):
    service['data']['history'] = history

    return service


def get_statement(document):
    array_service = get_services_from_document(document)
    array_history = get_histories_from_document(document)

    return map(set_history_to_service, array_service, array_history)
