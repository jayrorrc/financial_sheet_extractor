#!/usr/bin/env python
# encoding: utf-8
# vim: set syntax=python:

'''
BB transactions history
'''

import re
from models import utils as U
from bb.fi import transaction as T


def test_if_is_a_child(transaction_array):
    return re.search(r'Apl', transaction_array[0]) is not None


def test_if_is_a_collect(transaction_name):
    if isinstance(transaction_name, list):
        return U.test_collect(transaction_name[0])
    else:
        return U.test_collect(transaction_name)


def test_if_transaction_has_value(transaction_string):
    transaction_array = re.split(r'\s\s+', transaction_string.strip())

    return len(transaction_array) >= 2


def test_if_transaction_is_not_empty(transaction_string):
    return transaction_string.strip() != ''


def test_if_is_a_transactions(transaction_string):
    regex_result = re.search(r'(--+)|(DATA.*VALOR COTA)', transaction_string)

    return regex_result is None


def test_if_transaction_is_valid(transaction):
    is_transaction = test_if_is_a_transactions(transaction)
    is_not_empty = test_if_transaction_is_not_empty(transaction)
    has_value = test_if_transaction_has_value(transaction)

    return is_transaction and is_not_empty and has_value


def filter_transactions(array_transactions):
    return filter(test_if_transaction_is_valid, array_transactions)


def convert_transaction_string_to_array(transaction_string):
    return re.split(r'\s\s+', transaction_string.strip())


def build_history(history, transaction_string):
    transaction_array = convert_transaction_string_to_array(transaction_string)

    if not history:
        flag_ir_none = False
    else:
        flag_ir_none = history[-1].get('data').get('ir') <= 0

    flag_child = test_if_is_a_child(transaction_array)
    flag_collect = test_if_is_a_collect(transaction_array)

    if flag_child and not flag_collect:
        name_parent = history[-1].get('data').get('name')
        flag_collect = test_if_is_a_collect(name_parent)

    transaction = T.Transaction(
        transaction_array,
        transaction_string,
        [
            flag_collect,
            flag_ir_none,
            flag_child,
        ]
    )

    transaction_json = transaction.get_json()

    if not flag_child:
        history.append(transaction_json)
    else:
        history[-1].get('data').get('children').append(transaction_json)

    return history


get_array_of_transactions = lambda history_string: history_string.split('\n')


def separate_children(all_transaction, transaction_plus_children):
    array_children = get_array_of_transactions(transaction_plus_children)
    array_children = map(U.strip_str, array_children)

    if array_children[0] not in all_transaction:
        all_transaction += array_children

    return filter_transactions(all_transaction)


def get_array_of_all_transactions(array_valid_transactions):
    return reduce(separate_children, array_valid_transactions, [])


def reduce_from_array_to_history(array_valid_transactions):
    all_transaction = get_array_of_all_transactions(array_valid_transactions)

    return reduce(build_history, all_transaction, [])


def get_history(history_document):
    valid_transactions = filter_transactions(history_document)
    history = reduce_from_array_to_history(valid_transactions)

    return history
