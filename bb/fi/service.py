#!/usr/bin/env python
# encoding: utf-8
# vim: set syntax=python:

'''
Service in the BB statement standard
'''
import re
from models import service
from models import utils as U


class Service(service.Service, ):
    """Transactions of BB bank"""
    def __init__(self, service_array):
        self.service_array = service_array

        (name, cnpj) = self.get_cpnj_and_name()
        service_type = 'financial_fund'

        super(Service, self).__init__(
            name,
            cnpj,
            service_type,
            history
        )

    def get_cpnj_and_name(self):
        fund_name = re.sub(r'=\s+', '', self.service_array[0])
        fund_name = re.split(r'\s\s+', fund_name)[0]
        fund_name = U.strip_and_lower(fund_name)

        fund_cnpj = self.remove_special_caracteres(self.service_array[1])

        return (fund_name, fund_cnpj)

    def remove_special_caracteres(self, cnpj):
        return re.sub(r'(\.)|(\/)|(-)', '', cnpj)

    def get_json(self):
        return super(Service, self).get_json()
