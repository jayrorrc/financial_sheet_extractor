#!/usr/bin/env python
# encoding: utf-8
# vim: set syntax=python:

'''
Transactions in the BB statement standard
'''
import re
from models import transaction
from models import utils as U


class Transaction(transaction.Transaction):
    """Transactions of BB bank"""
    def __init__(self, transaction_array, transaction_string, flags):
        self.transaction_array = transaction_array
        self.transaction_string = transaction_string
        self.flag_collect = flags[0]
        self.flag_ir_none = flags[1]
        self.flag_child = flags[2]

        description = 'investment'
        date = self.get_date()
        name = self.get_name()
        [value, ir, iof, quotes] = self.get_values()

        super(Transaction, self).__init__(
            [
                description,
                date,
                name,
                value,
                ir,
                iof,
                quotes
            ]
        )

    def get_date(self):
        regex_result = re.findall(U.REGEX_DATE, self.transaction_array[0])

        if regex_result:
            return regex_result[0]

        if self.flag_child:
            if self.flag_collect:
                return self.transaction_array[0].split(' ')[1]

            return self.transaction_array[1]

    def __extract_name(self):
        if self.flag_child:
            return self.transaction_array[0]

        if re.search(r'[a-zA-Z]', self.transaction_array[0]) is not None:
            regex_result = re.split(U.REGEX_DATE, self.transaction_array[0])
            return regex_result[-1]

        return self.transaction_array[1]

    def get_name(self):
        name_full = self.__extract_name()
        name_full = U.strip_and_lower(name_full)
        name_first = name_full.split(' ')[0]

        if not self.flag_child and self.flag_collect:
            return name_full

        return name_first

    def get_values(self):
        regex_result = re.findall(U.REGEX_VALUE, self.transaction_string)
        array_values = map(lambda x: x[0], regex_result)

        value = ir = iof = quotes = None

        if self.flag_collect:
            ir = array_values[0]
        else:
            value = array_values[0]

        if len(array_values) is 1:
            pass
        elif len(array_values) is 2:
            quotes = array_values[-1]
        elif self.flag_child:
            quotes = array_values[-1]

            if len(array_values) is 3:
                val = array_values[-2]

                if len(val) < 40:
                    ir = val
                else:
                    iof = val

            else:
                ir = array_values[1]

                if U.convert_to_float(quotes) == 0:
                    iof = array_values[-3]
                    quotes = array_values[-2]
                else:
                    iof = array_values[-2]
        else:
            quotes = array_values[-2]

            if len(array_values) is 3:
                quotes = array_values[-2]
            elif len(array_values) is 4:
                val = array_values[-3]

                if len(val) < 40:
                    ir = val
                else:
                    iof = val
            else:
                ir = array_values[1]
                iof = array_values[-3]

        return map(U.convert_to_float, [value, ir, iof, quotes])

    def get_json(self):
        return super(Transaction, self).get_json()
