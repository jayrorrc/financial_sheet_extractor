'''
JSOM default to service
'''


class Service(object):
    def __init__(self, name, cnpj, description, history):
        super(Service, self).__init__()

        self.name = name
        self.cnpj = cnpj
        self.description = description
        self.history = history

    def get_json(self):
        return {
            'data_type': 'service',
            'data': {
                'name': self.name,
                'cnpj': self.cnpj,
                'service_type': self.description,
                'history': self.history
            }
        }
