'''
JSOM default to transaction
'''


class Transaction(object):
    def __init__(self, args):
        super(Transaction, self).__init__()

        self.description = args[0]
        self.date = args[1]
        self.name = args[2]
        self.value = args[3]
        self.ir = args[4]
        self.iof = args[5]
        self.quotes = args[6]
        self.children = []

    def get_json(self):
        return {
            'data_type': 'transaction',
            'data': {
                'transaction_type': self.description,
                'date': self.date,
                'name': self.name,
                'value': self.value,
                'ir': self.ir,
                'iof': self.iof,
                'num_quotes': self.quotes,
                'children': self.children
            }
        }
