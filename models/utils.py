#!/usr/bin/env python
# encoding: utf-8
# vim: set syntax=python:

'''
Generic functions to pattern data types
'''


import os
import re
import json


REGEX_DATE = r'([0-3][0-9]\/[0-1][0-9]\/2[0-9]{3})'
REGEX_VALUE = r'(\s+([0-9]+.|)+[0-9]+,[0-9]+)'
PATH_CURRENT = os.getcwd()
PATH_SUMARIES = PATH_CURRENT + '/test/doc/sumaries/'
PATH_JSONS = PATH_CURRENT + '/test/doc/jsons/'

strip_str = lambda string: string.strip()
strip_and_lower = lambda string: strip_str(string).lower()
concat_strings = lambda str1, str2: str1 + str2
remove_dot_from_value = lambda valueString: reduce(concat_strings, valueString)


def test_collect(string):
    return re.search(r'cobran(.{1})a', strip_and_lower(string)) is not None


def convert_to_float(string):
    if string is None:
        return None
    else:
        string = strip_str(string)

        if ',' in string:
            value = remove_dot_from_value(string.split('.'))
            value = value.replace(',', '.')
        else:
            value = string

        return float(value)

def get_statement_json(statement):
    '''
    Convert statement to JSON
    '''

    statement_json = json.dumps(
        statement,
        indent=2,
        sort_keys=True,
        encoding='latin1'
    )

    return statement_json.encode('utf8')

def format_dict(statement):
    return json.loads(get_statement_json(statement))
