import os
import json
import unittest
from app import APP


class FlaskrAppTests(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    @classmethod
    def tearDownClass(cls):
        pass

    def setUp(self):
        # creates a test client
        self.app = APP.test_client()
        # propagate the exceptions to the test client
        self.app.testing = True

    def tearDown(self):
        pass

    def test_status_code_get_bb_funds_infos_without_head(self):
        print 'test_status_code_get_bb_funds_infos_without_head()\t\t<========= actual test code'
        print ''

        result = self.app.get('/bb/fi')

        self.assertEqual(result.status_code, 400)

    def test_message_get_bb_funds_infos_without_head(self):
        print 'test_message_get_bb_funds_infos_without_head()\t\t\t<========= actual test code'
        print ''

        result = self.app.get('/bb/fi')
        result_data = json.loads(result.data)

        message_expeted = {
            "msg": "header 'path' not find or malformed"
        }

        self.assertEqual(result_data, message_expeted)

    def test_status_code_get_bb_funds_infos_with_wrong_path(self):
        print 'test_status_code_get_bb_funds_infos_with_wrong_path()\t\t<========= actual test code'
        print ''

        headers = {
            'path': '/wrong/path'
        }

        result = self.app.get('/bb/fi', headers=headers)
        self.assertEqual(result.status_code, 400)

    def test_message_get_bb_funds_infos_with_wrong_path(self):
        print 'test_message_get_bb_funds_infos_with_wrong_path()\t\t<========= actual test code'
        print ''

        headers = {
            'path': '/wrong/path'
        }

        result = self.app.get('/bb/fi', headers=headers)
        result_data = json.loads(result.data)

        message_expeted = {
            "msg": "header 'path' not find or malformed"
        }

        self.assertEqual(result_data, message_expeted)

    def test_status_code_get_bb_funds_infos_with_correct_path(self):
        print 'test_status_code_get_bb_funds_infos_with_correct_path()\t<========= actual test code'
        print ''

        current_path = os.getcwd()
        sumary_path = current_path + '/test/doc/sumaries/document'

        print 'current path:\t' + current_path
        print 'sumary path:\t' + sumary_path
        print ''

        headers = {
            'path': sumary_path
        }

        result = self.app.get('/bb/fi', headers=headers)
        self.assertEqual(result.status_code, 200)

    def test_data_of_get_bb_funds_infos_with_correct_path(self):
        print 'test_data_of_get_bb_funds_infos_with_correct_path()\t\t<========= actual test code'
        print ''

        current_path = os.getcwd()
        sumary_path = current_path + '/test/doc/sumaries/document'
        json_path = current_path + '/test/doc/jsons/document.json'

        print 'current path:\t' + current_path
        print 'sumary path:\t' + sumary_path
        print 'json path:\t' + json_path
        print ''

        headers = {
            'path': sumary_path
        }

        result = self.app.get('/bb/fi', headers=headers)
        result_data = json.loads(result.data)

        with open(json_path) as json_file:
            json_expected = json.load(json_file)

        self.assertEqual(result_data, json_expected)
