#!/usr/bin/env python
# encoding: utf-8
# vim: set syntax=python:

import json
from nose.tools import assert_equals
from models import utils as U
from bb.fi import extractor


def test_get_bb_renda_fixa_500_infos():
    print 'test_get_bb_renda_fixa_500_infos()\t<========= actual test code'
    print ''

    fund_name = 'bb_renda_fixa_500'
    fund_sumary_path = U.PATH_SUMARIES + fund_name
    sumary_file = open(fund_sumary_path, 'rb')
    sumary = sumary_file.read()
    funds_infos_response = U.format_dict(extractor.get_statement(sumary))

    json_expected_path = U.PATH_JSONS  + fund_name + '.json'

    with open(json_expected_path) as json_file:
        json_expected = json.load(json_file)

    assert_equals(funds_infos_response, json_expected)


def test_get_bb_renda_fixa_lp_100_infos():
    print 'test_get_bb_renda_fixa_lp_100_infos()\t<========= actual test code'
    print ''

    fund_name = 'bb_renda_fixa_lp_100'
    fund_sumary_path = U.PATH_SUMARIES + fund_name
    sumary_file = open(fund_sumary_path, 'rb')
    sumary = sumary_file.read()
    funds_infos_response = U.format_dict(extractor.get_statement(sumary))

    json_expected_path = U.PATH_JSONS  + fund_name + '.json'

    with open(json_expected_path) as json_file:
        json_expected = json.load(json_file)

    assert_equals(funds_infos_response, json_expected)


def test_get_bb_renda_fixa_lp_100_2_infos():
    print 'test_get_bb_renda_fixa_lp_100_2_infos()\t<========= actual test code'
    print ''

    fund_name = 'bb_renda_fixa_lp_100_2'
    fund_sumary_path = U.PATH_SUMARIES + fund_name
    sumary_file = open(fund_sumary_path, 'rb')
    sumary = sumary_file.read()
    funds_infos_response = U.format_dict(extractor.get_statement(sumary))

    json_expected_path = U.PATH_JSONS  + fund_name + '.json'

    with open(json_expected_path) as json_file:
        json_expected = json.load(json_file)

    assert_equals(funds_infos_response, json_expected)


def test_get_bb_renda_fixa_lp_100_3_infos():
    print 'test_get_bb_renda_fixa_lp_100_3_infos()\t<========= actual test code'
    print ''

    fund_name = 'bb_renda_fixa_lp_100_3'
    fund_sumary_path = U.PATH_SUMARIES + fund_name
    sumary_file = open(fund_sumary_path, 'rb')
    sumary = sumary_file.read()
    funds_infos_response = U.format_dict(extractor.get_statement(sumary))

    json_expected_path = U.PATH_JSONS  + fund_name + '.json'

    with open(json_expected_path) as json_file:
        json_expected = json.load(json_file)

    assert_equals(funds_infos_response, json_expected)


def test_get_bb_renda_fixa_lp_100_4_infos():
    print 'test_get_bb_renda_fixa_lp_100_4_infos()\t<========= actual test code'
    print ''

    fund_name = 'bb_renda_fixa_lp_100_4'
    fund_sumary_path = PATH_SUMARIES + fund_name
    sumary_file = open(fund_sumary_path, 'rb')
    sumary = sumary_file.read()
    funds_infos_response = json.loads(extractor.get_statement_json(sumary))

    json_expected_path = PATH_JSONS  + fund_name + '.json'

    with open(json_expected_path) as json_file:
        json_expected = json.load(json_file)

    assert_equals(funds_infos_response, json_expected)


def test_get_bb_mm_macro_lp_200_infos():
    print 'test_get_bb_mm_macro_lp_200_infos()\t<========= actual test code'
    print ''

    fund_name = 'bb_mm_macro_lp_200'
    fund_sumary_path = U.PATH_SUMARIES + fund_name
    sumary_file = open(fund_sumary_path, 'rb')
    sumary = sumary_file.read()
    funds_infos_response = U.format_dict(extractor.get_statement(sumary))

    json_expected_path = U.PATH_JSONS  + fund_name + '.json'

    with open(json_expected_path) as json_file:
        json_expected = json.load(json_file)

    assert_equals(funds_infos_response, json_expected)


def test_get_bb_rf_lp_ind_5_mil_infos():
    print 'test_get_bb_rf_lp_ind_5_mil_infos()\t<========= actual test code'
    print ''

    fund_name = 'bb_rf_lp_ind_5_mil'
    fund_sumary_path = U.PATH_SUMARIES + fund_name
    sumary_file = open(fund_sumary_path, 'rb')
    sumary = sumary_file.read()
    funds_infos_response = U.format_dict(extractor.get_statement(sumary))

    json_expected_path = U.PATH_JSONS  + fund_name + '.json'

    with open(json_expected_path) as json_file:
        json_expected = json.load(json_file)

    assert_equals(funds_infos_response, json_expected)


def test_get_bb_renda_fixa_cp_200_infos():
    print 'test_get_bb_renda_fixa_cp_200_infos()\t<========= actual test code'
    print ''

    fund_name = 'bb_renda_fixa_cp_200'
    fund_sumary_path = U.PATH_SUMARIES + fund_name
    sumary_file = open(fund_sumary_path, 'rb')
    sumary = sumary_file.read()
    funds_infos_response = U.format_dict(extractor.get_statement(sumary))

    json_expected_path = U.PATH_JSONS  + fund_name + '.json'

    with open(json_expected_path) as json_file:
        json_expected = json.load(json_file)

    assert_equals(funds_infos_response, json_expected)


def test_get_bb_acoes_bb_infos():
    print 'test_get_bb_acoes_bb_infos()\t\t<========= actual test code'
    print ''

    fund_name = 'bb_acoes_bb'
    fund_sumary_path = U.PATH_SUMARIES + fund_name
    sumary_file = open(fund_sumary_path, 'rb')
    sumary = sumary_file.read()
    funds_infos_response = U.format_dict(extractor.get_statement(sumary))

    json_expected_path = U.PATH_JSONS  + fund_name + '.json'

    with open(json_expected_path) as json_file:
        json_expected = json.load(json_file)

    assert_equals(funds_infos_response, json_expected)
