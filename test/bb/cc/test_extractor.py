#!/usr/bin/env python
# encoding: utf-8
# vim: set syntax=python:

import json
from nose.tools import assert_equals
from models import utils as U
from bb.cc import extractor


def test_get_extrato_20170707_infos():
    print 'test_get_extrato_20170707_infos()\t<========= actual test code'
    print ''

    cc_name = 'extrato_20170707'
    fund_sumary_path = U.PATH_SUMARIES + cc_name
    sumary_file = open(fund_sumary_path, 'rb')
    sumary = sumary_file.read()

    funds_infos_response = U.format_dict(extractor.get_statement(sumary))

    json_expected_path = U.PATH_JSONS  + cc_name + '.json'

    with open(json_expected_path) as json_file:
        json_expected = json.load(json_file)

    assert_equals(funds_infos_response, json_expected)
