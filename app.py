'''
API to extract informations from banks statements
'''

from os import path
from flask import Flask
from flask import request
from models import utils as U
from bb.fi import extractor as ext_fi
from bb.cc import extractor as ext_cc

APP = Flask(__name__)

def get_json(path_file, extractor):
    '''
    Test if file path is valid and return JSON
    '''

    funds_infos = '{"msg": "header \'path\' not find or malformed"}'
    status = 400

    if path_file is not None and path.isfile(path_file):
        document_file = open(path_file, 'rb')
        document = document_file.read()

        statement = extractor.get_statement(document)
        funds_infos = U.get_statement_json(statement)

        status = 200

    response = APP.response_class(
        response=funds_infos,
        status=status,
        mimetype='application/json'
    )

    return response


@APP.route('/bb/fi', methods=['GET'])
def get_bb_funds_infos():
    '''
    Route to extract informations from financials funds's
    statements of BB
    '''

    path_file = request.headers.get('path')

    return get_json(path_file, ext_fi)


@APP.route('/bb/cc', methods=['GET'])
def get_bb_cc_infos():
    '''
    Route to extract informations from account statments of BB
    '''

    path_file = request.headers.get('path')

    return get_json(path_file, ext_cc)


if __name__ == '__main__':
    APP.run(debug=True)
